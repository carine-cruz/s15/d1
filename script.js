/*
	Operator
		- Assignment
		- Arithmetic
		- Compound Assignment

		- Comparison
		- Logical
*/

/* Assignment Operator ( = )
	- use to assign a value to a varialbe

*/


/*	let a = 14;
	console.log(a);

	//rea-assign value
	a = 7;
	console.log(a);

	let b = a;
	console.log(a, b);*/

/* Arithmetic Operator

	Example:
*/

	//addition operator (+)
		// console.log(20+10);

	//subtraction operator (-)
		// console.log(20-10);

	//multiplication (*)
		// console.log(8*3);

	//division operator (/)
		// console.log(25 / 5);

	//modulo operator (%) -- gets the remainder
		// console.log(100 % 3);

/* Increment and Decrement */
/*	let c = 30;

	//prefix
	console.log(c)	//30
	console.log(++c); //31
	console.log(c); //31

	console.log(--c);	//30
	console.log(c)		//31

	//suffix
	console.log(c++);	//30
	console.log(c--);	//31
	console.log(c);		//30*/

// Mini Activity -----------
// 1. Perform Arithmetic Operation on two numbers.
// 2. Perform Arithmetic Operation on two variables.
// 3. Multiply a number to the difference of two numbers.
// 4. Multiply a number to the difference of two variables.

/*console.log("1st activity:");
console.log(10*5);

let a = 7;
let b = 5;
let c = 10;

console.log(`2nd activity:`);
console.log(a+b);

console.log(`3rd activity:`);
console.log(5*(10-2));

//solution #4
console.log(c*(a-b));*/

// --- Compount Assignment Operator ----
// 			-perform arithmetic operation
//			-asisgning back the variable to the variable

	//Example:


/*	let f = 15;
	//---Addition Assignment Operator (+=)---
	console.log(f += 3); //18
	console.log(f += 3); //21
	console.log(f += 3); //24

	//f = f + 3
	//f = 18

	//f = f + 3
	//f = 18 + 3
	//f = 21

	console.log(f + 3); //27
	console.log(f + 3); //27
	console.log(f + 3); //27

	//---Subtraction Assignment Operator (-=)---
	console.log(f -= 2) // 22
	console.log(f -= 2) // 20
	console.log(f -= 5) // 15
	console.log(f);

	//---Multiplication Assignment Operator (*=)---
	console.log (f *= 2); //30
	console.log (f *= 3); //90
	console.log (f *= 4); //360
	console.log(f); //360

	//---Division Assignment Operator (/=)---
	console.log(f /= 10); //36
	console.log(f /= 4); //9
	console.log(f /= 3); //3
	console.log(f);	//3

	//---Modulo Assignment Operator (%=)---
	f = 200;
	console.log(f %= 8); //2
	console.log(f %= 1); //0
	*/


	/* 
		Selection Control Structure
			- if else statement
			- switch case

		If statement

			Syntax:

				if(condition){
					statement
				}
	*/

/*		let age = 20;

		if(age>18){
			console.log(`You are allowed to enter.`);
		} else {
			console.log(`You are not allowed to enter.`)
		}
*/

/* ---Miniactivity--- */

/*
let height

function heightFeedback(height){


	if(height<150){
		console.log(`Below 150cm`);
	} else {
		console.log(`Above 150cm`);
	}
}

//heightFeedback(130);
let myHeight = prompt(`Enter your height`);
heightFeedback(myHeight);
*/

/* Mini Activity 2 */

/*function NameChecker(name){
	if (name=="Romeo"){
		console.log("Juliet");
	} else {
		console.log("Hamlet");
	}
}

let Name = prompt(`Enter name:`);
NameChecker(Name);
*/

	/* 

		if.. else if..else statement 

	*/

/*	let price = 120;

	if(price <= 20)
	{
		console.log(`Affordable`);
	}
	else if(price<=100)
	{
		console.log(`Pricey but still affordable.`);
	}
	else if(price<=200)
	{
		console.log(`Not affordable at the moment.`)
	}
	else
	{
		console.log(`Not affordable at all.`)
	}
*/


/* Mini Activity 3

Determine the typhoon intensity with the following data:

1. windspeed of 30, display not a typhoon
*/
/*
function typhoonInd(windSpeed){
	if(windSpeed<=30){
		console.log(`Not a typhoon.`);
	} else if(windSpeed<=61) {
		console.log(`A tropical depression is detected`);
	} else if(windSpeed>61 && windSpeed<=88){
		console.log(`A topical storm is detected.`);
	} else if(windSpeed>88 && windSpeed<=117){
		console.log(`A severe tropical storm is detected.`);
	} else{
		console.log(`Cannot identify storm detected.`);
	}
}

typhoonInd(prompt(`Enter windspeed`));
*/

	/*
		Ternary Operator
			-shorthand of if else statement
			- ? = if
			- : = else

			syntax:

				(condition) ? <statement> : <statement>

	let myAge = 18;

	(myAge >= 18) ? console.log(`You are allowed`) : console.log(`You are not allowed here`);

	*/

	/*
		Switch Statement

		syntax:
			switch(condition){
				case <category>: <statement/s>
								break;
				case <category>: <statement/s>
								break;
				default: <statement/s>
			}
	*/

/*	let num = 1;

	switch(num){
		case 1: console.log(`1`);
			break;
		case 2: console.log(`2`);
			break;
		case 3: console.log(`3`);
			break;
		case 4: console.log(`4`);
			break;
		default: console.log(`not found`);
	}
*/

/*
	let user = prompt(`Enter your role`);
	switch(user){
		case `admin`: console.log(`has authorization`);
			break;
		case `employee`: console.log(`ask for authorization`);
			break;
		default: console.log(`no access`);
	}
*/
/*
	let age=prompt(`Enter age`);

	switch(age){
		case age <= 5: console.log(`toddler`);
			break;
		case age >= 6 && age <=17: console.log(`teenager`);
			break;
		case age >= 18: console.log(`adult`);
			break;
		default: console.log(`Old enough`);
			break;
	}
*/
	function color(){

		let text = document.getElementById('input').value;
		let element = document.getElementById('hello');

		switch(text){
			case `red`: element.style.color = `red`
				break;
			case `blue`: element.style.color = `blue`
				break;
			case `green`: element.style.color = `green`
				break;
			default: element.style.color = `pink`
		}
	}
